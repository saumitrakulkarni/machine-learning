# -*- coding: utf-8 -*-
"""
@author: Saumitra Kulkarni
"""
import numpy as np
import matplotlib.pyplot as plt
from forward_propagation import forward_propagation
from backward_propagation import backward_propagation

class Deep_NN():
    
    def __init__(self, layers_dims: list, lr = 0.0075, itr = 3000, print_cost=False):
        # Number of layers
        self.layers_dims = layers_dims
        # Number of layers in the network
        self.L = len(self.layers_dims) 
        # Number of iterations
        self.itr = itr
        # Learning rate of stochastic gradient descent
        self.lr = lr
        # To print costs
        self.print_cost = print_cost

        
    # GRADED FUNCTION: initialize_parameters_deep
    def initialize_parameters(self):
        """
        Arguments:
        layer_dims -- python array (list) containing the dimensions of each layer in our network
        
        Returns:
        parameters -- python dictionary containing your parameters "W1", "b1", ..., "WL", "bL":
                        Wl -- weight matrix of shape (layer_dims[l], layer_dims[l-1])
                        bl -- bias vector of shape (layer_dims[l], 1)
        """
        
        np.random.seed(1)
        parameters = {}
    
        for l in range(1, self.L):
            parameters['W' + str(l)] = np.random.randn(self.layers_dims[l], self.layers_dims[l-1]) / np.sqrt(self.layers_dims[l-1])
            parameters['b' + str(l)] = np.zeros((self.layers_dims[l], 1))
            
            assert(parameters['W' + str(l)].shape == (self.layers_dims[l], self.layers_dims[l - 1]))
            assert(parameters['b' + str(l)].shape == (self.layers_dims[l], 1))
    
            
        return parameters
    
    
    # GRADED FUNCTION: compute_cost
    def compute_cost(self, A, Y):
        """
        Implement the cost function defined by equation (7).
    
        Arguments:
        AL -- probability vector corresponding to your label predictions, shape (1, number of examples)
        Y -- true "label" vector (for example: containing 0 if non-cat, 1 if cat), shape (1, number of examples)
    
        Returns:
        cost -- cross-entropy cost
        """
        
        m = Y.shape[1]
    
        # Compute loss from aL and y.
        cost = (1./m) * (-np.dot(Y,np.log(A).T) - np.dot(1-Y, np.log(1-A).T))
        # (-1/self.m) * np.sum(np.multiply(Y.T, np.log(A)) + np.multiply((1 - Y.T), np.log(1 - A)))
        
        cost = np.squeeze(cost)
        assert(cost.shape == ())
        
        return cost
    
    # GRADED FUNCTION: update_parameters
    def update_parameters(self, params, grads):
        """
        Update parameters using gradient descent
        
        Arguments:
        params -- python dictionary containing your parameters 
        grads -- python dictionary containing your gradients, output of L_model_backward
        
        Returns:
        parameters -- python dictionary containing your updated parameters 
                      parameters["W" + str(l)] = ... 
                      parameters["b" + str(l)] = ...
        """
        parameters = params.copy()
        
        L = len(parameters) // 2
    
        # Update rule for each parameter.
        for l in range(L):
            parameters["W" + str(l + 1)] = parameters["W" + str(l + 1)] - self.lr * grads["dW" + str(l + 1)]
            parameters["b" + str(l + 1)] = parameters["b" + str(l + 1)] - self.lr * grads["db" + str(l + 1)]
    
        return parameters
    
    def predict(self, X, parameters):
        """
        This function is used to predict the results of a  L-layer neural network.
        
        Arguments:
        X -- data set of examples you would like to label
        parameters -- parameters of the trained model
        
        Returns:
        p -- predictions for the given dataset X
        """
        
        y_predict = np.zeros((1, X.shape[1]))
        
        # Forward propagation
        probas, caches = forward_propagation(X, parameters)
    
        # convert probas to 0/1 predictions
        for i in range(0, probas.shape[1]):
            if probas[0,i] > 0.5:
                y_predict[0,i] = 1
            else:
                y_predict[0,i] = 0
        
        #print results
        #print ("predictions: " + str(y_predict))
        #print ("true labels: " + str(y))
            
        return y_predict
    
    
    # GRADED FUNCTION: L_layer_model
    def fit(self, X, Y):
        """
        Implements a L-layer neural network: [LINEAR->RELU]*(L-1)->LINEAR->SIGMOID.
        
        Arguments:
        X -- data, numpy array of shape (num_px * num_px * 3, number of examples)
        Y -- true "label" vector (containing 0 if cat, 1 if non-cat), of shape (1, number of examples)
        layers_dims -- list containing the input size and each layer size, of length (number of layers + 1).
        learning_rate -- learning rate of the gradient descent update rule
        num_iterations -- number of iterations of the optimization loop
        print_cost -- if True, it prints the cost every 100 steps
        
        Returns:
        parameters -- parameters learnt by the model. They can then be used to predict.
        """
    
        np.random.seed(1)
        costs = [] # keep track of cost
        
        # Parameters initialization
        parameters = self.initialize_parameters()
                
        # print(parameters)
        
        # Loop (gradient descent)
        for i in range(self.itr):
    
            # Forward propagation: [LINEAR -> RELU]*(L-1) -> LINEAR -> SIGMOID
            A, caches = forward_propagation(X, parameters)
            
            # Compute cost
            cost = self.compute_cost(A, Y)
    
            # Backward propagation
            grads = backward_propagation(A, Y, caches)

            # Update parameters
            parameters = self.update_parameters(parameters, grads)
             
            # Print the cost every 100 iterations
            if self.print_cost and i % 100 == 0 or i == self.itr - 1:
                print(f"Cost after iteration {i}: {np.squeeze(cost)}")
            if i % 100 == 0 or i == self.itr:
                costs.append(cost)
        
        return parameters, costs
    
    def score(self, X, y, parameters):
        """
        Model score based on comparison of expected and predicted value
    
        Parameters
        ----------
        X : ndarray
            Array of response variables
        y : ndarray
            Array of predictor variable
        
        Returns
        -------
        score : float 
            Model score in percentage
    
        """ 
        score = 100 - np.mean(np.abs(self.predict(X, parameters) - y)) * 100
        return score
    
    def plot_costs(self, costs):
        plt.plot(np.squeeze(costs))
        plt.ylabel('cost')
        plt.xlabel('iterations (per hundreds)')
        plt.title("Learning rate =" + str(self.lr))
        plt.show()
