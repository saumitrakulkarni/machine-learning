#!/usr/bin/env python
# coding: utf-8

# In[1]:


# -*- coding: utf-8 -*-
"""
@author: Saumitra Kulkarni
"""
import warnings
warnings.filterwarnings("ignore")

from sklearn import datasets
from sklearn.model_selection import train_test_split
from LogisticRegression_NN import LogisticRegression_NN

""" 
We will use Breast Cancer Data Set
 
Ref.: https://archive.ics.uci.edu/ml/datasets/breast+cancer
 
Note: sklean breast cancer data is used for training the model in order to 
classify / predict the breast cancer.
"""

# Load the dataset
bc = datasets.load_breast_cancer()

X = bc.data
y = bc.target

# Split the predictor and response into training and testing datasets
X_train, X_test, y_train, y_test = train_test_split(X, y, 
                                test_size=0.3, random_state=42, stratify=y)

# Print some information about training and testing datasets
print('Information about intial training and testing datasets')
print("Number of training examples: " + str(X_train.shape[0]))
print("Number of testing examples: " + str(X_test.shape[0]))

print("X_train shape: " + str(X_train.shape))
print("y_train shape: " + str(y_train.shape))
print("X_test shape: " + str(X_test.shape))
print("y_test shape: " + str(y_test.shape))
print('\n')

# Reshape the training and testing datasets
X_train_reshape = X_train.T
X_test_reshape = X_test.T
y_train_reshape = y_train.reshape(1,-1)
y_test_reshape = y_test.reshape(1,-1)

print('Information about modified training and testing datasets')
print("X_train_reshape shape: " + str(X_train_reshape.shape))
print("y_train_reshape shape: " + str(y_train_reshape.shape))
print("X_test_reshape shape: " + str(X_test_reshape.shape))
print("y_test_reshape shape: " + str(y_test_reshape.shape))
print('\n')

# Logistic regression classifier using Neural Networks
model = LogisticRegression_NN()

# Fit the model
print('Computing, please wait...')
d = model.fit(X_train_reshape, y_train_reshape)

# Training score
print("Train Score: ",model.score(X_train_reshape, y_train_reshape))

# Test score
print("Test Score: ",model.score(X_test_reshape, y_test_reshape))

