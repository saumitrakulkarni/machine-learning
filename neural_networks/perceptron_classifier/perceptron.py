# -*- coding: utf-8 -*-
"""
Created on Sat Nov 13 21:40:11 2021

@author: Saumitra Kulkarni
"""
import numpy as np

class Perceptron():
    """
    Perceptron classifier
    """
    def __init__(self, learning_rate = 0.01, no_of_itr = 10):
        # Number of iterations
        self.no_of_itr = no_of_itr
        # Learning rate of stochastic gradient descent
        self.learning_rate = learning_rate
    
    def step_func(self, x):
        """
        Activation function for perceptrons
        """
        return 1.0 if x >= 0.0 else 0.0
    
    def predict(self, x):
        """
        Calculate prediction or hypothesis
        """
        return self.step_func(np.dot(self.W.T, x))
    
    def fit(self, X, y):
        """
        Fit training data
        """
        self.W = np.zeros(1 + X.shape[1]) # Add 1 for bias
        self.errors = [] # Initialize variable for model score 
        for _ in range(self.no_of_itr): # Iterate for no of iterations
            # error = 0 # Initialize error at each iteration
            for i in range(y.shape[0]): # Iterate over each data point
                # Add 1 in X for bias
                x = np.insert(X[i], 0, 1)
                # Calculate change in weights 
                update = self.learning_rate * (y[i] - self.predict(x))
                # Compute weighted sum based on each data point
                self.W = self.W +  update * x
                
        return self
    
    def score(self, X, y):
        """
        Model score based on comparison of expected and predicted value
        """  
        error = 0 
        # Iterate over each data point
        for xi, yi in zip(X, y):
            # Add 1 in X for bias
            x = np.insert(xi, 0, 1)
            # Calculate outut for predictor
            output = self.predict(x)
            # Count misclassified data point when update is zero
            error += int(yi != output)
        # Return model score
        return (len(X) - error)/len(X)
                    
# Driver code       
if __name__ == '__main__':
    # The AND gate
    X = np.array([[0, 0],[0, 1],[1, 0],[1, 1]])
    y = np.array([0, 0, 0, 1])
    perceptron = Perceptron()
    perceptron.fit(X, y)
    print(perceptron.W)            
