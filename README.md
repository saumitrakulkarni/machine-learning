# Machine Learning

Python implementations of some of the fundamental Machine Learning models and algorithms from scratch.

The purpose of this project is not to produce as optimized and computationally efficient algorithms as possible but rather to study/present the inner workings of them in a transparent and accessible way.

Each directory contains two .py files: `main.py` and a library file. The library files contains codes and functions and `main.py` is a use case demonstration of library functions.  

This repository contains following codes:
- `neural_networks`
    - `logistic_regression` : Logistic Regression Classifier using Neural Network.
    - `perceptron_classifier` : Perceptron Classifier - A Simplest Neural Network.
    - `deep_neural_network` : A 3-layer Deep Neural Network.
